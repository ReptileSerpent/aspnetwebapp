﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AspNetWebApp.Models;

namespace AspNetWebApp.Data
{
    public class AspNetWebAppContext : DbContext
    {
        public AspNetWebAppContext (DbContextOptions<AspNetWebAppContext> options)
            : base(options)
        {
        }

        public DbSet<AspNetWebApp.Models.Category> Category { get; set; } = default!;

        public DbSet<AspNetWebApp.Models.Product>? Product { get; set; }
    }
}

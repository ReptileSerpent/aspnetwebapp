﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AspNetWebApp.Data;
using AspNetWebApp.Models;

namespace AspNetWebApp.Pages.Categories
{
    public class IndexModel : PageModel
    {
        private readonly AspNetWebApp.Data.AspNetWebAppContext _context;

        public IndexModel(AspNetWebApp.Data.AspNetWebAppContext context)
        {
            _context = context;
        }

        public IList<Category> Category { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Category != null)
            {
                Category = await _context.Category.ToListAsync();
            }
        }
    }
}

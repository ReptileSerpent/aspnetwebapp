﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using AspNetWebApp.Data;
using AspNetWebApp.Models;
using Microsoft.EntityFrameworkCore;

namespace AspNetWebApp.Pages.Categories
{
    public class CreateModel : PageModel
    {
        private readonly AspNetWebApp.Data.AspNetWebAppContext _context;

        public CreateModel(AspNetWebApp.Data.AspNetWebAppContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Category Category { get; set; } = default!;

        [BindProperty]
        public int TotalNewExtraAttributes { get; set; } = 0;


        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid || _context.Category == null || Category == null)
            {
                return Page();
            }

            var newExtraAttributeNames = new List<string>();
            if (TotalNewExtraAttributes > 0)
            {
                Category.ExtraAttributeDescriptions = new List<ExtraAttributeDescription>();
                for (var i = 1; i <= TotalNewExtraAttributes; i++)
                {
                    var newAttributeName = Request.Form["dynamic-field" + i];
                    Category.ExtraAttributeDescriptions.Add(new ExtraAttributeDescription() {
                        Name = newAttributeName,
                        Type = System.ComponentModel.DataAnnotations.DataType.Text
                    });
                }
            }

            _context.Category.Add(Category);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}

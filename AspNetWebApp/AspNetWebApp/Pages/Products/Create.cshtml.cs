﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using AspNetWebApp.Data;
using AspNetWebApp.Models;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace AspNetWebApp.Pages.Products
{
    public class CreateModel : PageModel
    {
        private readonly AspNetWebApp.Data.AspNetWebAppContext _context;

        public CreateModel(AspNetWebApp.Data.AspNetWebAppContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            IQueryable<string> categoryQuery = from category in _context.Category
                                               orderby category.Name
                                               select category.Name;

            Categories = new SelectList(categoryQuery.ToList());

            return Page();
        }

        [BindProperty]
        public Product Product { get; set; } = default!;

        public SelectList? Categories { get; set; }

        [BindProperty]
        public string SelectedCategory { get; set; } = string.Empty;

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid || _context.Product == null || Product == null)
            {
                return Page();
            }

            if (!string.IsNullOrEmpty(SelectedCategory))
            {
                IQueryable<string> categoryQuery = from category in _context.Category
                                                   orderby category.Name
                                                   select category.Name;

                Categories = new SelectList(categoryQuery.ToList());

                Product.Category = _context.Category.First(category => category.Name == SelectedCategory);
            }

            _context.Product.Add(Product);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}

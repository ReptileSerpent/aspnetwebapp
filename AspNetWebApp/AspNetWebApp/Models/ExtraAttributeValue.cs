﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApp.Models
{
    public class ExtraAttributeValue
    {
        public int Id { get; set; }
        [Required]
        public ExtraAttributeDescription? ExtraAttributeDescription { get; set; }
        [Required]
        public string Value { get; set; } = string.Empty;
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApp.Models
{
    public class ExtraAttributeDescription
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; } = string.Empty;
        [Required]
        public DataType Type { get; set; }
    }
}

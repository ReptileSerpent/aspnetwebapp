﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApp.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; } = string.Empty;
        public string Information { get; set; } = string.Empty;
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
        [DataType(DataType.ImageUrl)]
        public string Image { get; set; } = string.Empty;
        public Category? Category { get; set; }
        public ICollection<ExtraAttributeValue>? ExtraAttributeValues { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace AspNetWebApp.Models
{
    public class Category
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; } = string.Empty;
        public ICollection<ExtraAttributeDescription>? ExtraAttributeDescriptions { get; set; }
    }
}
